import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


public class Selenium {

    WebDriver webDriver = new ChromeDriver();

    void testGoogleDriver() {
        webDriver.get("http://www.anaesthetist.com/mnm/javascript/calc.htm");
        System.setProperty("webdriver.chrome.driver", "/chrome/chromedriver.exe");
        assertThat(webDriver.getTitle(), is(equalTo("A simple JavaScript calculator")));
    }

    void testDivision() {
        testGoogleDriver();

        webDriver.findElement(By.name("seven")).click();
        webDriver.findElement(By.name("two")).click();
        webDriver.findElement(By.name("div")).click();
        webDriver.findElement(By.name("nine")).click();
        webDriver.findElement(By.name("result")).click();

        String score = webDriver.findElement(By.name("Display")).getAttribute("value");

        if (score.equals("8")) {
            System.out.println("Passed");
        } else {
            System.out.println("Error");
            throw new AssertionError();
        }
    }

    void takeScreenshot() {
        try {
            TakesScreenshot takesScreenshot = ((TakesScreenshot) webDriver);
            File file = takesScreenshot.getScreenshotAs(OutputType.FILE);
            File filePath = new File("C://screen.png");
            FileUtils.copyFile(file, filePath);
        } catch (IOException e) {
                System.out.println("Failed.");
        }

        webDriver.quit();
    }

    public static void main(String []args) {
        Selenium selenium = new Selenium();
        selenium.testDivision();
        selenium.takeScreenshot();
    }
}
